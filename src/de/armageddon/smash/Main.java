package de.armageddon.smash;

import java.util.concurrent.ConcurrentHashMap;

import de.armageddon.smash.CMD.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


import de.armageddon.smash.events.Hit;
import de.armageddon.smash.listener.DoubleJump;
import de.armageddon.smash.listener.JoinListener;
import de.armageddon.smash.listener.LevelChangeListener;
import de.armageddon.smash.listener.StuffListener;
import de.armageddon.smash.manager.LocationManager;
import de.armageddon.smash.manager.MessageManager;
import de.armageddon.smash.manager.NameTagManager;

public class Main extends JavaPlugin {

	public Gamestate state;
	
	
	public ConcurrentHashMap<Player, Integer> jump_strength = new ConcurrentHashMap<>();
	public ConcurrentHashMap<Player, Integer> prozent = new ConcurrentHashMap<>();
	public ConcurrentHashMap<Player, Double> lives = new ConcurrentHashMap<>();
	public ConcurrentHashMap<Player, PlayerStatus> player = new ConcurrentHashMap<>();
	//Nachrichten
	public String pr;
	public String noperm;
	public String joinmessage;
	public String leavemessage;
	public String setlobby;
	public String nurspieler;
	public String lobbyargumente;
	public String errorspawn;
	public String spawngesetzt;
	public String gamestart;
	public String death;
	public String powerup;
	public Integer[] x;
	public Integer[] y;
	
	public int nb;
	
	//externe Classen
	public MessageManager mm;
	public LocationManager lm;
	public NameTagManager ntm;
	
	
	public static Main main;
	public int x1;
	public int x2;
	public int y1;
	public int y2;
	public int z;
	
	@Override
	public void onEnable() {
		main = this;
		
		lm = new LocationManager();
		mm = new MessageManager();
		ntm = new NameTagManager();
		
		mm.saveMsg();
		mm.register();
		mm.readMsg();
		
		
		Bukkit.getServer().getPluginManager().registerEvents(new DoubleJump(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new StuffListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new LevelChangeListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new JoinListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Hit(), this);

		this.getCommand("callevent").setExecutor(new CMD_callevent());
		this.getCommand("setlobby").setExecutor(new CMD_setlobby());
		this.getCommand("setspawn").setExecutor(new CMD_setspawn());
		this.getCommand("setitemscoords").setExecutor(new CMD_setitemscoords());
		this.getCommand("spawnitem").setExecutor(new CMD_spawnitem());
		this.getCommand("start").setExecutor(new CMD_start());
	}
}
