package de.armageddon.smash.events;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import de.armageddon.smash.Main;
import org.bukkit.util.Vector;

public class Hit implements Listener{

	Random rnd = new Random();
	
	@EventHandler
	public void OnHit(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();

			int alt = Main.main.prozent.get(p);
			int neu1 = rnd.nextInt(4);
			int neu2 = neu1 + 4;
			int neu = alt + neu2;
			Main.main.prozent.replace(p, neu);
			if (neu <= 40) {
				Main.main.ntm.prefix(p, "§0 | §a " + neu + "%");
			} else if (neu <= 80 && neu >= 41) {
				Main.main.ntm.prefix(p, "§0 | §2 " + neu + "%");
			} else if (neu <= 120 && neu >= 81) {
				Main.main.ntm.prefix(p, "§0 | §e " + neu + "%");
			} else if (neu <= 160 && neu >= 121) {
				Main.main.ntm.prefix(p, "§0 | §6 " + neu + "%");
			} else if (neu <= 200 && neu >= 161) {
				Main.main.ntm.prefix(p, "§0 | §c " + neu + "%");
			} else if (neu <= 240 && neu >= 201) {
				Main.main.ntm.prefix(p, "§0 | §4 " + neu + "%");
			} else if (neu >= 241) {
				p.setHealth(0.0D);
			}
            Vector v = e.getDamager().getLocation().getDirection().multiply(neu * 0.5f).setY(5f);
			p.setVelocity(v);
		} else {
			e.setCancelled(true);
		}
	}
	
}
