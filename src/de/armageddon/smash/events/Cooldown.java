package de.armageddon.smash.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.scheduler.BukkitTask;


public class Cooldown extends PlayerEvent {

	private Player player;
	BukkitTask coold;
	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public Cooldown(Player player) {
		super(player);
		this.player = player;
		cooldown();
	}

	public void cooldown() {
		coold = Bukkit.getScheduler().runTaskTimerAsynchronously(de.armageddon.smash.Main.main, () -> {
			if (de.armageddon.smash.Main.main.jump_strength.get(player) < 100) {
				int current = de.armageddon.smash.Main.main.jump_strength.get(player);
				de.armageddon.smash.Main.main.jump_strength.put(player, current + 1);
				player.setLevel(player.getLevel() + 1);
				player.setExp((float) ((float) player.getLevel() * 0.01));

			} else {
				stopCooldown();
			}
		}, 2, 2);
	}

	public void stopCooldown() {
		coold.cancel();
	}

}