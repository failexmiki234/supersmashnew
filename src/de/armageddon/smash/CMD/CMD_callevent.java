package de.armageddon.smash.CMD;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.armageddon.smash.events.Cooldown;

public class CMD_callevent implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		Bukkit.broadcastMessage("Calling event...");
		Bukkit.getPluginManager().callEvent(new Cooldown(null));
		return false;
	}

}
