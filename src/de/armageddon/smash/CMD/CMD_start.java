package de.armageddon.smash.CMD;

import de.armageddon.smash.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Random;

public class CMD_start implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender.hasPermission("ssm.startgame")) {
            Random rnd = new Random();
            Bukkit.broadcastMessage(Main.main.pr + " Starting game...");
            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                p.teleport(Main.main.lm.getSpawn(Math.abs((rnd.nextInt() % 9)) + 1));
            }
            return true;
        }
        return false;
    }
}
