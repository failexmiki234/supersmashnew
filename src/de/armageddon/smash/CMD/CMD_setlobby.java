package de.armageddon.smash.CMD;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.armageddon.smash.Main;


public class CMD_setlobby  implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			if(sender.hasPermission("ssm.setlobby") || sender.hasPermission("ssm.admin") || sender.hasPermission("ssm.dev")) {
				Player p = (Player) sender;
				if(args.length != 0) {
					sender.sendMessage(Main.main.pr + Main.main.lobbyargumente);
					return true;
				}
				Main.main.lm.setLocation("lobby", p.getLocation());
				p.sendMessage(Main.main.pr + Main.main.setlobby);
				return true;
			} else {
				sender.sendMessage(Main.main.pr + Main.main.noperm);
				return false;
			}
		} else {
			sender.sendMessage(Main.main.pr + Main.main.nurspieler);
		}
		return false;
	}
	
}
