package de.armageddon.smash.CMD;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.armageddon.smash.Main;
import de.armageddon.smash.manager.LocationManager;

public class CMD_setitemscoords implements CommandExecutor{

	LocationManager lm = new LocationManager();
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
			if (args.length == 0) {
				sender.sendMessage(Main.main.pr + "Du musst Intervalle von Koordinaten angeben und zwar so:");
				sender.sendMessage(Main.main.pr + "/setitemscoords x1 x2 y1 y2 z");
				sender.sendMessage(Main.main.pr + "bsp.: /setitemscoords 173 203 -80 -50 50");
				return false;
			} else if (args.length < 5) {
				sender.sendMessage(Main.main.pr + "Du hast nur " + args.length + " Argumente angegeben, du musst 4 angeben!");
				return false;
			} else {
				int x1 = Integer.parseInt(args[0]);
				int x2 = Integer.parseInt(args[1]);
				int y1 = Integer.parseInt(args[2]);
				int y2 = Integer.parseInt(args[3]);
				int z = Integer.parseInt(args[4]);
				int sizex;
				int sizey;
				
				//Hier möchte ich die Differenz zw. den Integern herausfinden bzw. aus wie vielen Bl�cken der bestimmte Bereich besteht.
				if (x1 - x2 < 0) {
					sizex = x2 - x1;
					Main.main.x = new Integer[sizex + 1];
					for (int i = 0; i <= sizex; i++) {
						Main.main.x[i] = x1 + i;
					}
				} else {
					sizex = x1 - x2;
					Main.main.x = new Integer[sizex + 1];
					for (int i = 0; i <= sizex; i++) {
						Main.main.x[i] = x2 + i;
					}
				}
				
				if (y1 - y2 < 0) {
					sizey = y2 - y1;
					Main.main.y = new Integer[sizey + 1];
					for (int i = 0; i <= sizey; i++) {
						Main.main.y[i] = y1 + i;
					}
						
				} else {
					sizey = y1 - y2;
					Main.main.y = new Integer[sizey + 1];
					for (int i = 0; i <= sizey; i++) {
						Main.main.y[i] = y2 + i;
					}
				}
				
				Bukkit.broadcastMessage(Main.main.pr + "Die Differenz von x: " + sizex);
				Bukkit.broadcastMessage(Main.main.pr + "Die Differenz von y: " + sizey);
				Bukkit.broadcastMessage(Main.main.pr + "X1: " + x1);
				Bukkit.broadcastMessage(Main.main.pr + "X2: " + x2);
				Bukkit.broadcastMessage(Main.main.pr + "Y1: " + y1);
				Bukkit.broadcastMessage(Main.main.pr + "Y2: " + y2);
				Bukkit.broadcastMessage(Main.main.pr + "Z: " + z);
				Main.main.x1 = x1;
				Main.main.x2 = x2;
				Main.main.y1 = y1;
				Main.main.y2 = y2;
				Main.main.z = z;
				
				lm.savetoConfig("x1", x1);
				lm.savetoConfig("x2", x2);
				lm.savetoConfig("y1", y1);
				lm.savetoConfig("y2", y2);
				lm.savetoConfig("z", z);

			}
		return true;
	}

}
