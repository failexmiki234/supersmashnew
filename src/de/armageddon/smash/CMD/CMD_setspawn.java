package de.armageddon.smash.CMD;


import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.armageddon.smash.Main;


public class CMD_setspawn implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("ssm.setspawn") || !sender.hasPermission("ssm.admin") || !sender.hasPermission("ssm.dev")) {
			sender.sendMessage(Main.main.pr + Main.main.noperm);
		} else {
			if (args.length == 1) {
				sender.sendMessage(Main.main.pr + Main.main.errorspawn);
				return false;
			} else {
				if (args.length == 1) {
					if (args[0].equals("1")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(1, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "1");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("2")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(2, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "2");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("3")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(3, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "3");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("4")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(4, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "4");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("5")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(5, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "5");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("6")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(6, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "6");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("7")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(7, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "7");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("8")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(8, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "8");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("9")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(9, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "9");
						p.sendMessage(Main.main.pr + nummer);
					} else if (args[0].equals("10")) {
						Player p = (Player) sender;
						Location loc = p.getLocation();
						Main.main.lm.setSpawn(10, loc);
						String nummer = Main.main.spawngesetzt.replace("{nummer}", "10");
						p.sendMessage(Main.main.pr + nummer);
					} else {
						sender.sendMessage(Main.main.pr + Main.main.errorspawn);
					}
				}
			
			}
		}

		return false;
	}
	
}
