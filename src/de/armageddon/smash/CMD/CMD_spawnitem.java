package de.armageddon.smash.CMD;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.armageddon.smash.Main;
import de.armageddon.smash.manager.LocationManager;

public class CMD_spawnitem implements CommandExecutor {
	LocationManager lm = new LocationManager();
	Random rnd = new Random();

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if (arg3.length != 0) {
			if (arg3[0].equals("0")) {
				loadConfig();
				addToArray();
				spawnRandom();
			}
		} else {
			spawnRandom();
		}
		return true;

	}

	public void loadConfig() {
		Main.main.x1 = lm.getfromConfig("x1");
		Main.main.x2 = lm.getfromConfig("x2");
		Main.main.y1 = lm.getfromConfig("y1");
		Main.main.y2 = lm.getfromConfig("y2");
		Main.main.z = lm.getfromConfig("z");
	}

	public void addToArray() {
		int sizex;
		int sizey;
		int x1 = Main.main.x1;
		int x2 = Main.main.x2;
		int y1 = Main.main.y1;
		int y2 = Main.main.y2;
		int z;
		if (x1 - x2 < 0) {
			sizex = x2 - x1;
			Main.main.x = new Integer[sizex + 1];
			for (int i = 0; i <= sizex; i++) {
				Main.main.x[i] = x1 + i;
			}
		} else {
			sizex = x1 - x2;
			Main.main.x = new Integer[sizex + 1];
			for (int i = 0; i <= sizex; i++) {
				Main.main.x[i] = x2 + i;
			}
		}

		if (y1 - y2 < 0) {
			sizey = y2 - y1;
			Main.main.y = new Integer[sizey + 1];
			for (int i = 0; i <= sizey; i++) {
				Main.main.y[i] = y1 + i;
			}

		} else {
			sizey = y1 - y2;
			Main.main.y = new Integer[sizey + 1];
			for (int i = 0; i <= sizey; i++) {
				Main.main.y[i] = y2 + i;
			}
		}
	}

	public void spawnRandom() {
		World w = Bukkit.getWorld("world");
		int Low = 0;
		int Highx = Main.main.x.length;
		int Highy = Main.main.y.length;
		int Resultx = rnd.nextInt(Highx - Low) + Low;
		int Resulty = rnd.nextInt(Highy - Low) + Low;
		Location loc = new Location(w, Main.main.x[Resultx], Main.main.z, Main.main.y[Resulty]);
		Location loc2 = loc.clone();
		loc2.setY(loc.getY() - 1);
		if (loc2.getBlock().getType() == Material.AIR) {
			Bukkit.broadcastMessage(Main.main.pr + "Block ist Luft!");
			Bukkit.broadcastMessage(loc2.getBlock().getType().toString());
		} else {
			Bukkit.broadcastMessage(Main.main.pr + "Block ist nicht Luft!");
			Bukkit.broadcastMessage(loc2.getBlock().getType().toString());
			w.dropItemNaturally(loc, new ItemStack(Material.YELLOW_FLOWER, 1));
		}
	}

}
