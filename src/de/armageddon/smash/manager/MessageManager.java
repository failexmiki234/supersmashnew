package de.armageddon.smash.manager;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.armageddon.smash.Main;


public class MessageManager {

	public File msgfile = new File("plugins/" + Main.main.getName(), "messages.yml");
	public FileConfiguration msg = YamlConfiguration.loadConfiguration(msgfile);
	
	public void register() {
		msg.options().copyDefaults(true);
		msg.addDefault("Prefix", "&7[&eSuperSmash&7] &r");
		msg.addDefault("joinmessage", "&a{player} ist beigetreten");
		msg.addDefault("leavemessage", "&4{player} hat das Spiel verlassen");
		msg.addDefault("nopermission", "&4Das darfst du nicht!");
		msg.addDefault("setlobby", "&aDu hast die &6Lobby &agesetzt!");
		msg.addDefault("nurspieler", "&aDu musst ein Spieler sein.");
		msg.addDefault("lobbyargumente", "&aDu musst &6/setlobby &anutzen!");
		msg.addDefault("errorspawn", "&4Nutze /setspawn <1>/<0> (<0|1|2>)");
		msg.addDefault("spawngesetzt", "&aDu hast den {nummer} Spawn gesetzt");
		
		saveMsg();

	}
	
	public void readMsg() {
		Main.main.pr = ChatColor.translateAlternateColorCodes('&', msg.getString("Prefix"));
		Main.main.joinmessage = ChatColor.translateAlternateColorCodes('&', msg.getString("joinmessage"));
		Main.main.leavemessage = ChatColor.translateAlternateColorCodes('&', msg.getString("leavemessage"));
		Main.main.noperm = ChatColor.translateAlternateColorCodes('&', msg.getString("nopermission"));
		Main.main.setlobby = ChatColor.translateAlternateColorCodes('&', msg.getString("setlobby"));
		Main.main.nurspieler = ChatColor.translateAlternateColorCodes('&', msg.getString("nurspieler"));
		Main.main.lobbyargumente = ChatColor.translateAlternateColorCodes('&', msg.getString("lobbyargumente"));
		Main.main.errorspawn = ChatColor.translateAlternateColorCodes('&', msg.getString("errorspawn"));
		Main.main.spawngesetzt = ChatColor.translateAlternateColorCodes('&', msg.getString("spawngesetzt"));
	}
	
	public void saveMsg() {
		try {
			msg.save(msgfile);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
}
