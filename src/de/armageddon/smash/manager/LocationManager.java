package de.armageddon.smash.manager;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.armageddon.smash.Main;



public class LocationManager {

	public File file = new File("plugins/" + Main.main.getName(), "locs.yml");
	public File config_f = new File("plugins/" + Main.main.getName(), "config.yml");
	public FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	public FileConfiguration config = YamlConfiguration.loadConfiguration(config_f);
	

	public void setLocation(String name, Location loc) {
		cfg.set(name + ".world", loc.getWorld().getName());
		cfg.set(name + ".X", loc.getX());
		cfg.set(name + ".Y", loc.getY());
		cfg.set(name + ".Z", loc.getZ());
		cfg.set(name + ".YAW", loc.getYaw());
		cfg.set(name + ".PITCH", loc.getPitch());
		saveCfg();
	}

	public Location getLocation(String name) {
		Location loc;
		try {
			World w = Bukkit.getWorld(cfg.getString(name + ".world"));
			double x = cfg.getDouble(name + ".X");
			double y = cfg.getDouble(name + ".Y");
			double z = cfg.getDouble(name + ".Z");
			loc = new Location(w, x, y, z);
			loc.setPitch(cfg.getInt(name + ".YAW"));
			loc.setPitch(cfg.getInt(name + ".PITCH"));

		} catch (Exception e) {
			loc = new Location(Bukkit.getWorlds().get(0), 0, 0, 0);
		}
		return loc;
	}
	
	public void setSpawn(int number, Location loc) {
		String name = "Spawn";
		cfg.set(name + "." + number + ".world", loc.getWorld().getName());
		cfg.set(name + "." + number + ".X", loc.getX());
		cfg.set(name + "." + number + ".Y", loc.getY());
		cfg.set(name + "." + number + ".Z", loc.getZ());
		cfg.set(name + "." + number + ".YAW", loc.getYaw());
		cfg.set(name + "." + number + ".PITCH", loc.getPitch());
		saveCfg();
	}
	
	public Location getSpawn(int number) {
		String name = "Spawn";
		World w = Main.main.getServer().getWorld(cfg.getString(name + "." + number + ".world"));
		double x = cfg.getDouble(name + "." + number + ".X");
		double y = cfg.getDouble(name + "." + number + ".Y");
		double z = cfg.getDouble(name + "." + number + ".Z");
		Location loc = new Location(w, x, y, z);
		loc.setPitch(cfg.getInt(name + "." + number + ".YAW"));
		loc.setPitch(cfg.getInt(name + "." + number + ".PITCH"));
		return loc;
	}
	
	public void savetoConfig(String coord, int coords) {
		config.set(coord, coords);
		saveCfg();
	}
	
	public int getfromConfig(String coord) {
		 return config.getInt(coord);
	}
	public void saveCfg() {
		try {
			cfg.save(file);
			config.save(config_f);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
