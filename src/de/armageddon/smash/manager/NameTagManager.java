package de.armageddon.smash.manager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class NameTagManager {

	public Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();

	public void prefix(Player p, String suffix)  {
		
		String suffix1 = suffix.replace("&",  "§").replace("_", " ");
		
		Team t = sb.getTeam(suffix);
		
		if(t == null) {
			t = sb.registerNewTeam(suffix);
			t.setSuffix(suffix1);
		}
		t.addPlayer(p);
		
		for(Player all : Bukkit.getOnlinePlayers()) {
			all.setScoreboard(sb);
		}
		
	}
	
}
