package de.armageddon.smash.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.armageddon.smash.Main;
import de.armageddon.smash.PlayerStatus;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R1.EnumClientCommand;
import net.minecraft.server.v1_8_R1.PacketPlayInClientCommand;

import java.util.Random;

public class JoinListener implements Listener {

	Location newspawn;
	Random rnd;

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		de.armageddon.smash.Main.main.jump_strength.put(e.getPlayer(), 100);
		de.armageddon.smash.Main.main.prozent.put(e.getPlayer(), 0);
		de.armageddon.smash.Main.main.lives.put(e.getPlayer(), 6.0);
		Main.main.player.put(e.getPlayer(), PlayerStatus.ALIVE);
		e.getPlayer().setLevel(100);
		Bukkit.broadcastMessage("Spieler gesetzt: " + e.getPlayer().getName() + " " + Main.main.nb);
		Main.main.nb++;
		e.getPlayer().setMaxHealth(6.0);
		e.getPlayer().setHealth(6.0);
		e.getPlayer().setExp(1);
		e.getPlayer().teleport(Main.main.lm.getLocation("lobby"));
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		de.armageddon.smash.Main.main.jump_strength.remove(e.getPlayer());
		de.armageddon.smash.Main.main.prozent.remove(e.getPlayer());
		Bukkit.broadcastMessage("Spieler entfernt. " + e.getPlayer().getName());
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		((CraftPlayer) e.getEntity()).getHandle().playerConnection
				.a(new PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN));
		de.armageddon.smash.Main.main.jump_strength.replace(e.getEntity(), 100);
		de.armageddon.smash.Main.main.prozent.replace(e.getEntity(), 0);
		de.armageddon.smash.Main.main.lives.replace(e.getEntity(), Main.main.lives.get(e.getEntity()) - 2.0);
		if (Main.main.lives.get(e.getEntity()) > 1.0D) {
			e.getEntity().setLevel(100);
			e.getEntity().setExp(1);
			if (Main.main.lives.get(e.getEntity()) > 1.0D) {
				e.getEntity().setMaxHealth(Main.main.lives.get(e.getEntity()));
				e.getEntity().setHealth(Main.main.lives.get(e.getEntity()));
			} else {
				e.getEntity().setMaxHealth(1.0);
				e.getEntity().setHealth(1.0);
			}
			rnd = new Random();
			int rand = (rnd.nextInt() % 9);
			Bukkit.broadcastMessage("Spawn Nummer " + rand);
			newspawn = Main.main.lm.getSpawn(Math.abs(rand) + 1);
			e.getEntity().teleport(newspawn);
			e.setDeathMessage(Main.main.pr + e.getEntity().getName() + " hat noch " + ChatColor.GOLD
					+ (Main.main.lives.get(e.getEntity()).intValue() / 2) + ChatColor.WHITE + " Leben übrig!");
		} else {
			Main.main.player.replace(e.getEntity(), PlayerStatus.DEAD);
			e.getEntity().setMaxHealth(20);
			e.getEntity().setHealth(20);
			e.getEntity().setLevel(1337);
			e.getEntity().setExp(0);
			e.setDeathMessage(
					Main.main.pr + e.getEntity().getName() + " hat sein letztes Leben verloren und ist raus!");
			e.getEntity().teleport(Main.main.lm.getLocation("lobby"));
		}
	}

}
