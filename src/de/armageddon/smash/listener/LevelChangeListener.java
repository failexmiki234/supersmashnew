package de.armageddon.smash.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLevelChangeEvent;

import de.armageddon.smash.events.Cooldown;

public class LevelChangeListener implements Listener{
	
	@EventHandler
	public void onLevelChange(PlayerLevelChangeEvent e) {
		if (e.getOldLevel() == 100) {
			Bukkit.getPluginManager().callEvent(new Cooldown(e.getPlayer()));
		}
	}

}
