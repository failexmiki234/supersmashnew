package de.armageddon.smash.listener;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

import de.armageddon.smash.Main;
import de.armageddon.smash.PlayerStatus;

public class DoubleJump implements Listener {

	@EventHandler
	public void onPlayerToggleFly(PlayerToggleFlightEvent e) {
		Player p = e.getPlayer();
		if ((p.getGameMode() == GameMode.CREATIVE) || (p.getGameMode() == GameMode.SPECTATOR)) {
			return;
		}
		if (Main.main.player.get(e.getPlayer()) == PlayerStatus.DEAD) {
			e.setCancelled(true);
			return;
		}
		e.setCancelled(true);
		p.setFlying(false);
		p.setAllowFlight(false);
		p.setFallDistance(0.0F);
		Location loc = p.getLocation();
		Vector v = loc.getDirection().multiply(1.1F).setY(1.1D);
		p.setVelocity(v);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if ((p.getGameMode() == GameMode.CREATIVE) || (p.getGameMode() == GameMode.SPECTATOR)) {
			return;
		}
		if ((p.isOnGround()) && (!p.getAllowFlight()) || (!p.getAllowFlight())) {
			if (de.armageddon.smash.Main.main.jump_strength.get(e.getPlayer()) < 40) {
			} else {
				if (Main.main.player.get(e.getPlayer()) == PlayerStatus.ALIVE) {
						p.setAllowFlight(true);
						int strength = de.armageddon.smash.Main.main.jump_strength.get(e.getPlayer());
						de.armageddon.smash.Main.main.jump_strength.replace(e.getPlayer(), strength - 40);
						e.getPlayer().setLevel(e.getPlayer().getLevel() - 40);
				}
			}
		}
	}
}
