package de.armageddon.smash.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class StuffListener implements Listener {

	@EventHandler
	public void onFall(EntityDamageEvent e) {
		e.setCancelled(e.getCause().equals(DamageCause.FALL));
	}

	@EventHandler
	public void noFood(FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e.getCause() == DamageCause.ENTITY_ATTACK) {
			e.setDamage(0);
		} else {
		}
	}
}
